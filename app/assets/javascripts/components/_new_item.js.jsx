const NewItem = (props) => {
  let formFields = {}
  return (
    <div>
    <input placeholder ="Enter name of Item" ref={input => formFields.name = input} />
    <input placeholder ="Enter a Description" ref={input => formFields.description = input} />
    <button onClick={ () => props.handleFormSubmit(formFields.name.value, formFields.description.value)}>Submit</button>
    </div>
  )
}
