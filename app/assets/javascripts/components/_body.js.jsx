class Body extends React.Component {
	constructor(props) {
		super(props);
		this.state = {items: []};
		this.handleFormSubmit = this.handleFormSubmit.bind(this)
		this.addNewItem = this.addNewItem.bind(this)
		this.handleDelete = this.handleDelete.bind(this)
		this.deleteItem = this.deleteItem.bind(this)
	}
	componentDidMount() {
		console.log("Component Mounted");
		$.getJSON('/api/v1/items.json', (response) => { this.setState({ items: response }) });
	}
	handleFormSubmit(name, description) {
		console.log(name, description);
		$.ajax({
			url: '/api/v1/items',
			type: "POST",
			data: {item: {name: name, description: description}},
			success: response => {
				this.addNewItem(response);
			}
		});
	}
	addNewItem (item){
		this.setState({
			items: this.state.items.concat(item)
		})
	}
	handleDelete(id) {
		$.ajax({
			url: 'api/v1/items/${id}',
			method: 'DELETE',
			success: response => {
				this.deleteItem(id)
			}
		})
	}
	deleteItem(id){
    newFruits = this.state.items.filter((item) => item.id !== id)
    this.setState({
      items: newItems
    })
  }
	render() {
		return(
			<div>
				<NewItem handleFormSubmit={this.handleFormSubmit} />
				<AllItems items = {this.state.items} handleDelete={this.handleDelete}/>
			</div>
		)
	}
}
